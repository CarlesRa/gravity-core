import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GravityFwRoutingModule } from './gravity-fw-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    GravityFwRoutingModule
  ]
})
export class GravityFwModule { }
